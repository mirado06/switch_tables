<?php

class UserManager extends DataAccess {

    private static $_instance = null;

    public static function get_instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new UserManager();
        }
        return self::$_instance;
    }

    public function get_database() {
        return $this->_database;
    }

    public function GetList() {
        $query = $this->_database->query('SELECT * FROM ' . $_SESSION['site_name'] . '_users');
        $query->execute();
        $datas = $query->fetchAll(PDO::FETCH_ASSOC);
        return $datas;
    }

    public function Get($id_user) {
        $query = $this->_database->query('SELECT * FROM ' . $_SESSION['site_name'] . '_users WHERE id_user = ' . $id_user);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($data !== FALSE) {
            return $data;
        } else {
            return NULL;
        }
    }

    public function Insert(User $testUser) {
        $query = $this->_database->prepare('INSERT INTO ' . $_SESSION['site_name'] . '_users SET lastname = :lastname, firstname = :firstname, email = :email, username = :username, password = :password, level = :level');
        $query->bindValue(':lastname', $testUser->get_lastname());
        $query->bindValue(':firstname', $testUser->get_firstname());
        $query->bindValue(':email', $testUser->get_email());
        $query->bindValue(':username', $testUser->get_username());
        $query->bindValue(':password', $testUser->get_password());
        $query->bindValue(':level', $testUser->get_level(), PDO::PARAM_INT);
        $query->execute();

        return $this->_database->lastInsertId();
    }

    public function Update(User $testUser, $id_user) {
        $query = $this->_database->prepare('UPDATE ' . $_SESSION['site_name'] . '_users SET id_user = :id_user, lastname = :lastname, firstname = :firstname, email = :email, username = :username, password = :password, level = :level WHERE id_user = :old_id_user');
        $query->bindValue(':id_user', $testUser->get_id_user(), PDO::PARAM_INT);
        $query->bindValue(':lastname', $testUser->get_lastname());
        $query->bindValue(':firstname', $testUser->get_firstname());
        $query->bindValue(':email', $testUser->get_email());
        $query->bindValue(':username', $testUser->get_username());
        $query->bindValue(':password', $testUser->get_password());
        $query->bindValue(':level', $testUser->get_level(), PDO::PARAM_INT);
        $query->bindValue(':old_id_user', $id_user, PDO::PARAM_INT);
        $query->execute();

        return $this->_database->lastInsertId();
    }

    public function Remove($test_users) {
        $type = gettype($test_users);
        if ($type === 'integer') {
            $id_user = $test_users;
        } else if ($type === 'object') {
            $class = get_class($test_users);
            if ($class === 'User') {
                $id_user = $id_user->get_id_user();
            }
        }

        $query = $this->_database->prepare('DELETE FROM ' . $_SESSION['site_name'] . '_users WHERE id_user = :id_user');
        $query->bindValue(':id_user', $id_user, PDO::PARAM_INT);
        $query->execute();
    }

    /* / OTHER / */
    public function authenticate($username, $password) {
        $query = $this->_database->prepare('SELECT * FROM ' . $_SESSION['site_name'] . '_users WHERE username = :username AND password = :password');
        $query->bindValue(':username', $username);
        $query->bindValue(':password', $password);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($data !== FALSE) {
            return $data;
        } else {
            return NULL;
        }
    }
}
