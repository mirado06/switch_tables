<?php

class DataAccess {

    protected $_database;

    public function __construct() {
        $this->_database = new PDO('mysql:host=localhost;dbname=comptabilite-2', 'root', 'root', array(PDO::ATTR_PERSISTENT => true, PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    }

}
