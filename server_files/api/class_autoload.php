<?php

function load_class($class) {
    require $class . '.class.php';
}

spl_autoload_register('load_class');
