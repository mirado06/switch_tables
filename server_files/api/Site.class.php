<?php

class Site {

    private $_id_site;
    private $_name;

    const GET_SITES = 'GET_SITES';
    const CREATE_SITE = 'CREATE_SITE';
    const SWITCH_TABLES = 'SWITCH_TABLES';

    public function __construct(array $datas = NULL) {

        if (isset($datas)) {
            $this->hydrate($datas);
        }
    }

    public function hydrate(array $datas) {
        foreach ($datas as $key => $value) {
            $methodName = 'set_' . $key;
            if (method_exists($this, $methodName)) {
                $this->$methodName($value);
            }
        }
    }

    public function get_id_site() {
        return $this->_id_site;
    }

    public function set_id_site($id_site) {
        $this->_id_site = $id_site;
    }

    public function get_name() {
        return $this->_name;
    }

    public function set_name($name) {
        $this->_name = $name;
    }

}
