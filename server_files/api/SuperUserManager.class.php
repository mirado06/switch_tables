<?php

class SuperUserManager extends DataAccess {

    private static $_instance = null;

    public static function get_instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new SuperUserManager();
        }
        return self::$_instance;
    }

    public function get_database() {
        return $this->_database;
    }

    public function GetList() {
        $query = $this->_database->query('SELECT * FROM super_users');
        $query->execute();
        $datas = $query->fetchAll(PDO::FETCH_ASSOC);
        return $datas;
    }

    public function Get($id_super_user) {
        $query = $this->_database->query('SELECT * FROM super_users WHERE id_super_user = ' . $id_super_user);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($data !== FALSE) {
            return $data;
        } else {
            return NULL;
        }
    }

    public function Insert(SuperUser $superUser) {
        $query = $this->_database->prepare('INSERT INTO super_users SET login = :login, password = :password, level = :level, id_site = :id_site');
        $query->bindValue(':login', $superUser->get_login());
        $query->bindValue(':password', $superUser->get_password());
        $query->bindValue(':level', $superUser->get_level());
        $query->bindValue(':id_site', $superUser->get_id_site(), PDO::PARAM_INT);
        $query->execute();

        return $this->_database->lastInsertId();
    }

    public function Update(SuperUser $superUser, $id_super_user) {
        $query = $this->_database->prepare('UPDATE super_users SET id_super_user = :id_super_user, login = :login, password = :password, level = :level, id_site = :id_site WHERE id_super_user = :old_id_super_user');
        $query->bindValue(':id_super_user', $superUser->get_id_super_user(), PDO::PARAM_INT);
        $query->bindValue(':login', $superUser->get_login());
        $query->bindValue(':password', $superUser->get_password());
        $query->bindValue(':level', $superUser->get_level());
        $query->bindValue(':id_site', $superUser->get_id_site(), PDO::PARAM_INT);
        $query->bindValue(':old_id_super_user', $id_super_user, PDO::PARAM_INT);
        $query->execute();

        return $this->_database->lastInsertId();
    }

    public function Remove($super_users) {
        $type = gettype($super_users);
        if ($type === 'integer') {
            $id_super_user = $super_users;
        } else if ($type === 'object') {
            $class = get_class($super_users);
            if ($class === 'SuperUser') {
                $id_super_user = $id_super_user->get_id_super_user();
            }
        }

        $query = $this->_database->prepare('DELETE FROM super_users WHERE id_super_user = :id_super_user');
        $query->bindValue(':id_super_user', $id_super_user, PDO::PARAM_INT);
        $query->execute();
    }

    /* / OTHER / */

    public function authenticate($username, $password) {
        $query = $this->_database->prepare('SELECT * FROM super_users WHERE login = :username AND password = :password');
        $query->bindValue(':username', $username);
        $query->bindValue(':password', $password);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($data !== FALSE) {
            return $data;
        } else {
            return NULL;
        }
    }

}
