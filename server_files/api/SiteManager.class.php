<?php

class SiteManager extends DataAccess {

    private static $_instance = null;

    public static function get_instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new SiteManager();
        }
        return self::$_instance;
    }

    public function get_database() {
        return $this->_database;
    }

    public function GetList() {
        $query = $this->_database->query('SELECT * FROM sites WHERE id_site != 1');
        $query->execute();
        $datas = $query->fetchAll(PDO::FETCH_ASSOC);
        return $datas;
    }

    public function Get($id_site) {
        $query = $this->_database->query('SELECT * FROM sites WHERE id_site = ' . $id_site);
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($data !== FALSE) {
            return $data;
        } else {
            return NULL;
        }
    }

    public function Insert(Site $site) {
        $query = $this->_database->prepare('INSERT INTO sites SET name = :name');
        $query->bindValue(':name', $site->get_name());
        $query->execute();

        return $this->_database->lastInsertId();
    }

    public function Update(Site $site, $id_site) {
        $query = $this->_database->prepare('UPDATE sites SET id_site = :id_site, name = :name WHERE id_site = :old_id_site');
        $query->bindValue(':id_site', $site->get_id_site(), PDO::PARAM_INT);
        $query->bindValue(':name', $site->get_name());
        $query->bindValue(':old_id_site', $id_site, PDO::PARAM_INT);
        $query->execute();

        return $this->_database->lastInsertId();
    }

    public function Remove($sites) {
        $type = gettype($sites);
        if ($type === 'integer') {
            $id_site = $sites;
        } else if ($type === 'object') {
            $class = get_class($sites);
            if ($class === 'Site') {
                $id_site = $id_site->get_id_site();
            }
        }

        $query = $this->_database->prepare('DELETE FROM sites WHERE id_site = :id_site');
        $query->bindValue(':id_site', $id_site, PDO::PARAM_INT);
        $query->execute();
    }

    /* /    CREATE TABLES / */

    public function SiteExists($site_name) {
        $query = $this->_database->query("SELECT * FROM sites WHERE name = '" . $site_name . "'");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if ($data !== FALSE) {
            return $data;
        } else {
            return NULL;
        }
    }

    public function CreateTables($prefix) {
        try {
            $accounts = $prefix . '_accounts';
            $sql_accounts = <<<EOSQL
                CREATE TABLE $accounts (id_account INT NOT NULL AUTO_INCREMENT, number VARCHAR(200) NOT NULL, 
                    description VARCHAR(500) NOT NULL, parent INT DEFAULT NULL, 
                    alterable INT NOT NULL DEFAULT 0, countable INT NOT NULL,
                    PRIMARY KEY (id_account),
                    FOREIGN KEY (parent) REFERENCES $accounts(id_account))
EOSQL;
            $users = $prefix . '_users';
            $sql_users = <<<EOSQL
                CREATE TABLE $users (id_user INT NOT NULL AUTO_INCREMENT, lastname VARCHAR(500) NOT NULL, 
                    firstname VARCHAR(500) NOT NULL, email VARCHAR(500) NOT NULL, username VARCHAR(500) NOT NULL, 
                    password VARCHAR(500) NOT NULL, level INT NOT NULL,
                    PRIMARY KEY (id_user))
EOSQL;
            $operations = $prefix . '_operations';
            $sql_operations = <<<EOSQL
                CREATE TABLE $operations(id_operation INT NOT NULL AUTO_INCREMENT, id_user INT NOT NULL, date TIMESTAMP NOT NULL,	
                    PRIMARY KEY (id_operation), FOREIGN KEY (id_user) REFERENCES $users(id_user))
EOSQL;
            $entries = $prefix . '_entries';
            $sql_entries = <<<EOSQL
                CREATE TABLE $entries (id_entry INT NOT NULL AUTO_INCREMENT, id_account INT NOT NULL, 
                    id_operation INT NOT NULL, wording VARCHAR(500) NOT NULL,
                    credit_or_debit CHAR(1) NOT NULL DEFAULT 0, amount DOUBLE NOT NULL,
                    PRIMARY KEY (id_entry),
                    FOREIGN KEY (id_account) REFERENCES $accounts(id_account),
                    FOREIGN KEY (id_operation) REFERENCES $operations(id_operation))
EOSQL;
            $invoices = $prefix . '_invoices';
            $sql_invoices = <<<EOSQL
                CREATE TABLE $invoices (id_invoice INT NOT NULL AUTO_INCREMENT, reference VARCHAR(500) NOT NULL,
                    link VARCHAR(500) NOT NULL, date TIMESTAMP NOT NULL, tva DOUBLE,
                    PRIMARY KEY (id_invoice))
EOSQL;
            $invoices_details = $prefix . '_invoices_details';
            $sql_invoices_details = <<<EOSQL
                CREATE TABLE $invoices_details (id_invoice_details INT NOT NULL AUTO_INCREMENT,
                    id_invoice INT NOT NULL, designation VARCHAR(500) NOT NULL, unit_price INT NOT NULL,
                    quantity INT NOT NULL,
                    PRIMARY KEY (id_invoice_details),
                    FOREIGN KEY (id_invoice) REFERENCES $invoices(id_invoice))
EOSQL;
            $entry_invoices = $prefix . '_entry_invoices';
            $sql_entry_invoices = <<<EOSQL
                CREATE TABLE $entry_invoices (
                    id_entry_invoice INT NOT NULL AUTO_INCREMENT, id_entry INT NOT NULL,
                    id_invoice INT NOT NULL,
                    PRIMARY KEY (id_entry_invoice),
                    FOREIGN KEY (id_invoice) REFERENCES $invoices(id_invoice),
                    FOREIGN KEY (id_entry) REFERENCES $entries(id_entry))
EOSQL;

            $r = $this->_database->exec($sql_accounts);
            $r = $this->_database->exec($sql_users);
            $r = $this->_database->exec($sql_operations);
            $r = $this->_database->exec($sql_entries);
            $r = $this->_database->exec($sql_invoices);
            $r = $this->_database->exec($sql_invoices_details);
            $r = $this->_database->exec($sql_entry_invoices);

            return 'OK';
        } catch (PDOException $e) {
            return 'KO';
        }
    }

    public function CreateTables2($prefix) {
        try {
            $sql = 'CREATE TABLE ' . $prefix . '_accounts LIKE accounts; '
                    . 'CREATE TABLE ' . $prefix . '_users LIKE users; '
                    . 'CREATE TABLE ' . $prefix . '_operations LIKE operations; '
                    . 'CREATE TABLE ' . $prefix . '_entries LIKE entries; '
                    . 'CREATE TABLE ' . $prefix . '_invoices LIKE invoices; '
                    . 'CREATE TABLE ' . $prefix . '_invoices_details LIKE invoices_details; '
                    . 'CREATE TABLE ' . $prefix . '_entry_invoices LIKE entry_invoices; '
                    . 'INSERT INTO ' . $prefix . '_accounts SELECT * FROM accounts; ';
            $this->_database->query($sql);
            return 'OK';
        } catch (PDOException $e) {
            return 'KO';
        }
    }

}
