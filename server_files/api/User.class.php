<?php

class User {

    private $_id_user;
    private $_lastname;
    private $_firstname;
    private $_email;
    private $_username;
    private $_password;
    private $_level;

    public function __construct(array $datas = NULL) {

        if (isset($datas)) {
            $this->hydrate($datas);
        }
    }

    public function hydrate(array $datas) {
        foreach ($datas as $key => $value) {
            $methodName = 'set_' . $key;
            if (method_exists($this, $methodName)) {
                $this->$methodName($value);
            }
        }
    }

    public function get_id_user() {
        return $this->_id_user;
    }

    public function set_id_user($id_user) {
        $this->_id_user = $id_user;
    }

    public function get_lastname() {
        return $this->_lastname;
    }

    public function set_lastname($lastname) {
        $this->_lastname = $lastname;
    }

    public function get_firstname() {
        return $this->_firstname;
    }

    public function set_firstname($firstname) {
        $this->_firstname = $firstname;
    }

    public function get_email() {
        return $this->_email;
    }

    public function set_email($email) {
        $this->_email = $email;
    }

    public function get_username() {
        return $this->_username;
    }

    public function set_username($username) {
        $this->_username = $username;
    }

    public function get_password() {
        return $this->_password;
    }

    public function set_password($password) {
        $this->_password = $password;
    }

    public function get_level() {
        return $this->_level;
    }

    public function set_level($level) {
        $this->_level = $level;
    }

}
