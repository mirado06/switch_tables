<?php

session_start();
require_once 'class_autoload.php';
$cmd = GetParam('cmd');

$UserManager = UserManager::get_instance();
$SiteManager = SiteManager::get_instance();
$SuperUserManager = SuperUserManager::get_instance();

function GetParam($param) {
    if (!isset($_REQUEST[$param])) {
        $response['message'] = 'Missing Parameter';
        echo json_encode($response);
        exit;
    }
    return $_REQUEST[$param];
}

switch ($cmd) {
    case Site::GET_SITES:
        $response = $SiteManager->GetList();
        break;

    case 'AUTHENTICATE':
        $response['state'] = 'KO';
        $username = GetParam('username');
        $password = md5(GetParam('password'));

        $verify_user = $SuperUserManager->authenticate($username, $password);
        if ($verify_user != NULL) {
            $response['state'] = 'OK';
            $response['user'] = $verify_user[0];
            $_SESSION['super_user'] = $verify_user[0];
        }
        break;

    case 'IS_CONNECTED':
        $response['state'] = 'KO';
        if (isset($_SESSION['super_user'])) {
            $response['state'] = 'OK';
            $response['super_user'] = $_SESSION['super_user'];
        }
        break;

    case Site::CREATE_SITE:
        $response['state'] = 'KO';
        $response['msg'] = 'LE SITE EXISTE DEJA';
        $site_name = GetParam('site_name');
        if (!$SiteManager->SiteExists($site_name)) {
            $state_create_tables = $SiteManager->CreateTables2($site_name);
            $response['msg'] = 'IL Y A EU UNE ERREUR DANS LA CREATION DES TABLES';
            if ($state_create_tables === "OK") {
                $site['name'] = $site_name;
                $site_object = new Site($site);
                $response['id_site'] = $SiteManager->Insert($site_object);
                $response['state'] = 'OK';
                $response['msg'] = 'SITE CREEE AVEC SUCCES';
                $response['list'] = $SiteManager->GetList();
            }
        }
        break;

    case Site::SWITCH_TABLES:
        $response['state'] = 'KO';
        $response['msg'] = 'LE SITE N EXISTE PAS';

        $username = GetParam('username');
        $password = md5(GetParam('password'));
        $site_name = GetParam('site_name');

        if ($SiteManager->SiteExists($site_name) != NULL) {
            $_SESSION['site_name'] = $site_name;
            $response['msg'] = 'VEUILLEZ VERIFIER VOS IDENTIFIANTS SUR ' . $site_name;

            $verify_user = $UserManager->authenticate($username, $password);
            if ($verify_user != NULL) {
                $response['state'] = 'OK';
                $response['user'] = $verify_user[0];
                $response['msg'] = 'CONNECTE AVEC SUCCESS';
            }
        }
        break;

    case 'DESTROY_SESSION':
        session_destroy();
        break;

    default:
        break;
}

echo json_encode($response);

