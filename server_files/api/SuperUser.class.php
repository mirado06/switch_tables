<?php

class SuperUser {

    private $_id_super_user;
    private $_login;
    private $_password;
    private $_level;
    private $_id_site;

    public function __construct(array $datas = NULL) {

        if (isset($datas)) {
            $this->hydrate($datas);
        }
    }

    public function hydrate(array $datas) {
        foreach ($datas as $key => $value) {
            $methodName = 'set_' . $key;
            if (method_exists($this, $methodName)) {
                $this->$methodName($value);
            }
        }
    }

    public function get_id_super_user() {
        return $this->_id_super_user;
    }

    public function set_id_super_user($id_super_user) {
        $this->_id_super_user = $id_super_user;
    }

    public function get_login() {
        return $this->_login;
    }

    public function set_login($login) {
        $this->_login = $login;
    }

    public function get_password() {
        return $this->_password;
    }

    public function set_password($password) {
        $this->_password = $password;
    }

    public function get_level() {
        return $this->_level;
    }

    public function set_level($level) {
        $this->_level = $level;
    }

    public function get_id_site() {
        return $this->_id_site;
    }

    public function set_id_site($id_site) {
        $this->_id_site = $id_site;
    }

}
