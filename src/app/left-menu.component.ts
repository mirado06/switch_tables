import { Component, OnInit } from '@angular/core';

const MENU_ITEMS = [
    {
        title: 'Accueil',
        ico_url: './img/ico_accueil.png',
        link: "/dashboard",
        children: <any>[],
    },
    {
        title: 'Gestion Sites',
        ico_url: './img/ico_articles.png',
        link: '',
        children: [
            {
                title: 'Ajouter Site',
                link: "/add-site"
            }
        ],
    },
    {
        title: 'Statistiques',
        ico_url: './img/ico_stats.png',
        link: "/",
        children: <any>[],
    },
    {
        title: "Centre d'aide",
        ico_url: './img/ico_aide.png',
        link: "/",
        children: <any>[],
    },
    {
        title: "Deconnexion",
        ico_url: './img/ico_aide.png',
        link: "/logout",
        children: <any>[],
    }
];

@Component({
  selector: 'left-menu',
  templateUrl: './templates/left-menu.component.html',
  styleUrls: ['./styles/left-menu.component.css']
})

export class LeftMenuComponent implements OnInit {
    name = 'Left Menu';
    current_item = 0;
    menu_items = MENU_ITEMS;

    toggle(idx: number): void {
        if (this.current_item === idx) {
            // Folding
            this.current_item = -1;
        } else {
            // Expanding
            this.current_item = idx;
        }
    };
    ngOnInit(): void {

    }

}
