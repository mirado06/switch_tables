import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AppComponent} from './app.component';
import {NgForm} from '@angular/forms';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {EventEmitter} from "@angular/core";

import {SiteService} from "./services/site.service";
import {Site} from "./classes/site";

import {LoginService} from './services/login.service';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';

@Component({
    providers: [SiteService, LoginService],
    selector: 'accounts-list',
    styleUrls: ['./styles/site.component.css'],
    templateUrl: './templates/site.component.html'
})

export class SiteComponent implements OnInit {
    name = 'Gestion des comptes';

    showForm: boolean = true;

    sites_list: Site[];

    site: Site = {
        id_site: 1,
        name: ''
    };
    site_form: FormGroup;

    ngOnInit(): void {
        this.checkCredentials();
        this.getSitesList();
        this.createForm();
    }

    constructor(
        private router: Router,
        private account_service: SiteService,
        private fb: FormBuilder,
        private app_component: AppComponent,
        private login_service: LoginService
    ) {
        this.createForm();
    }
    createForm() {
        this.site_form = this.fb.group({
            name_ctrl: ['', Validators.required]
        });
    }

    /*/ INSERER NOUVEAU COMPTE /*/
    insertSite(): void {
        this.site.name = this.site_form.get('name_ctrl').value;
        this.account_service.insertSite(this.site).then(sites_list => this.sites_list = sites_list);
    }

    getSitesList(): void {
        this.account_service.getSitesList().then(sites_list => this.sites_list = sites_list);
    }

    /*/ GESTION BOUTONS /*/
    ValidateButton(): void {
        if (this.site_form.status === 'VALID') {
            this.insertSite();
            this.Reset();
        }
    }

    Reset(): void {
        this.site_form.reset();
        this.showForm = false;
        setTimeout(() => {
            this.createForm();
            this.showForm = true;
        });
    }

    putIndicationMessageColor(state: boolean) {
        if (state === true) {
            return "#09C6AB";
        } else {
            return "#F23557";
        }
    }

    checkCredentials() {
        this.login_service.isConnected().subscribe(res => {
            if (res['state'] !== "OK") {
                this.router.navigate(['/login'], {});
            }
        });
    }

}