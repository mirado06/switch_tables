import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LogoutService} from './services/logout.service';
import {LoginService} from './services/login.service';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';
@Component({
    providers: [LoginService],
    selector: 'logout',
    templateUrl: './templates/logout.component.html'
})
export class LogoutComponent implements OnInit {

    constructor(
        private router: Router,
        private logout_service: LogoutService,
         private login_service: LoginService
    ) {}

    ngOnInit() {
        this.logout();
    }

    logout() {
        this.logout_service.logout();
        this.router.navigate(['/login'], {});
    }
    
     checkCredentials() {
        this.login_service.isConnected().subscribe(res => {
            if (res['state'] === "OK") {
                this.router.navigate(['/dashboard'], {});
            }
            else {
                this.router.navigate(['/login'], {});
            }
        });
    }
}
