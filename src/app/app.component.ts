import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './templates/app.component.html',
    styleUrls: ['./styles/app.component.css']
})

export class AppComponent {
    title = 'app';

    m_opened: boolean;
    ml_opened: boolean;
    show_dialog: boolean = false;

    hide_wrapper(param: boolean): void {
        this.show_dialog = param;
    }

    ngOnInit(): void {
        this.m_opened = true;
        this.ml_opened = false;
    }

    fold(): void {
        if (this.m_opened) {
            this.m_opened = !this.m_opened;
            this.ml_opened = !this.ml_opened;
        } else {
            this.ml_opened = !this.ml_opened;
            this.m_opened = !this.m_opened;
        }
    }

    loginHide(): void {
        this.m_opened = false;
        this.ml_opened = false;
    }

    loginShow(): void {
        this.m_opened = true;
        this.ml_opened = false;
    }
}
