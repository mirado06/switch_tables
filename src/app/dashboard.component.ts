import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {AppComponent} from './app.component';

import {LoginService} from './services/login.service';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';

@Component({
    providers: [LoginService],
    selector: 'dashboard',
    styleUrls: ['./styles/dashboard.component.css'],
    templateUrl: './templates/dashboard.component.html',
})


export class DashboardComponent implements OnInit {
    name = 'Dashboard';

    constructor(
        private app_component: AppComponent,
        private router: Router,
        private login_service: LoginService
    ) {}

    ngOnInit(): void {
        this.checkCredentials();
    }

    checkCredentials() {
        this.login_service.isConnected().subscribe(res => {
            this.login_service.isConnected().subscribe(res => {
                if (res['state'] !== "OK") {
                    this.router.navigate(['/login'], {});
                }
            });
        });
    }
}
