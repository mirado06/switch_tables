import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {
    private api_url_authenticate = './api/server.php?cmd=AUTHENTICATE';
    private api_url_is_connected = './api/server.php?cmd=IS_CONNECTED';

    constructor(
        private http: Http) {}
    
    login(need_for_logging: any) {
        return this.http.get(this.api_url_authenticate
            + '&username=' + need_for_logging['username']
            + '&password=' + need_for_logging['password'])
            .map(resp => {
                resp.json();
                return resp.json();
            });
    }
    
    isConnected():any{
        return this.http.get(this.api_url_is_connected)
            .map(resp => {
                resp.json();
                return resp.json();
            });
    }

}


