import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {Response, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {Site} from './../classes/site';

@Injectable()
export class SiteService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private api_url_get_sites = './api/server.php?cmd=GET_SITES';
    private api_url_create_site = './api/server.php?cmd=CREATE_SITE';

    constructor(private http: Http) {}

    insertSite(ecriture: Site): Promise<Site[]> {
        return this.http.get(
            this.api_url_create_site
            + '&site_name=' + ecriture.name
        )
            .toPromise()
            .then(response => response.json()['list'] as Site[])
            .catch(this.handleError);
    }

    getSitesList(): Promise<Site[]> {
        return this.http.get(this.api_url_get_sites)
            .toPromise()
            .then(response => response.json() as Site[])
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}