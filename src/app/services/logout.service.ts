import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class LogoutService {

    private api_url_destroy_sessions = './api/server.php?cmd=DESTROY_SESSION';

    constructor(private http: Http) {}

    logout(): Promise<string> {
        return this.http.get(this.api_url_destroy_sessions)
            .toPromise()
            .then(response => response.json() as string)
            .catch(this.handleError);
    }
    
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
