import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {MaterialModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MdInputModule} from '@angular/material';
import {HttpModule} from '@angular/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import 'hammerjs';

import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard.component';
import {LeftMenuComponent} from './left-menu.component';
import {LoginComponent} from './login.component';
import {LogoutComponent} from './logout.component';
import {SiteComponent} from './site.component';
import {LoginService} from './services/login.service';
import {LogoutService} from './services/logout.service';
import {SiteService} from './services/site.service';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        LeftMenuComponent,
        LoginComponent,
        LogoutComponent,
        SiteComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        MaterialModule,
        BrowserAnimationsModule,
        MdInputModule,
        HttpModule,
        NoopAnimationsModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule
    ],
    providers: [
        LoginService,
        LogoutService,
        SiteService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
