import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FormControl} from '@angular/forms';

import {AppComponent} from './app.component';

import {LoginService} from './services/login.service';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';

@Component({
    providers: [LoginService],
    templateUrl: './templates/login.component.html',
    styleUrls: ['./styles/login.component.css']
})

export class LoginComponent implements OnInit {
    public msg = '';

    form: FormGroup;
    society_ctrl: FormControl;

    need_for_logging: any = {
        username: '',
        password: ''
    }

    constructor(
        private app_component: AppComponent,
        private router: Router,
        private login_service: LoginService,
        private fb: FormBuilder
    ) {
        if (localStorage.getItem('user')) {
            this.router.navigate(['/dashboard'], {});
        }
        this.createForm();
    }

    createForm(): void {
        this.form = this.fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    ngOnInit() {
        this.checkCredentials();
    }

    login() {
        this.need_for_logging['username'] = this.form.get('username').value;
        this.need_for_logging['password'] = this.form.get('password').value;
        this.login_service.login(this.need_for_logging)
            .subscribe(res => {
                if (res['state'] === "OK") {
                    this.app_component.loginShow();
                    this.router.navigate(['/dashboard'], {});
                }
                else {
                    this.msg = 'Identifiants invalides';
                }
            });
    }

    checkCredentials() {
        this.login_service.isConnected().subscribe(res => {
            if (res['state'] === "OK") {
                this.app_component.loginShow();
                this.router.navigate(['/dashboard'], {});
            }
            else {
                this.app_component.loginHide();
                this.router.navigate(['/login'], {});
            }
        });
    }

    putIndicationMessageColor() {
    }
}